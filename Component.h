#pragma once

class Entity;

class Component {
public:
	Entity* owner;
	virtual ~Component() {}
	virtual void Initialise() {}
	virtual void Update(float deltaTime) {}
	virtual void Render() {}
};