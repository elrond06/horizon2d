#include <algorithm>
#include "EntityManager.h"

void EntityManager::ClearData() {
	for (auto& entity : entities) {
		entity->Destroy();
	}
}

bool EntityManager::HasNoEntity() {
	return entities.size() == 0;
}

void EntityManager::Update(float deltaTime) {
	for (auto& entity : entities) {
		entity->Update(deltaTime);
	}
}

void EntityManager::Render() {
	for (int layerNumber = 0; layerNumber < NUM_LAYERS; layerNumber++) {
		for (auto& entity : GetEntitiesByLayer(static_cast<LayerType>(layerNumber))) {
			entity->Render();
		}
	}
}

Entity& EntityManager::AddEntity(std::string entityName, LayerType layer) {
	Entity* entity = new Entity(*this, entityName, layer);
	entities.emplace_back(entity);

	return *entity;
}

std::vector<Entity*> EntityManager::GetEntities() const {
	return entities;
}

std::vector<Entity*> EntityManager::GetEntitiesByLayer(LayerType layer) const {
	std::vector<Entity*> entitiesInLayer;
	std::copy_if(entities.begin(), entities.end(), std::back_inserter(entitiesInLayer),
		[layer](Entity* entity) { 
			return entity->layer == layer; 
		}
	);

	return entitiesInLayer;
}

unsigned int EntityManager::GetEntityCount() {
	return entities.size();
}

void EntityManager::ListAllEntities() const {
	unsigned int i = 0;

	for (auto& entity : entities) {
		std::cout << "Entity[" << i << "]:" << entity->name << std::endl;
		entity->ListAllComponents();
		i++;
	}
}
