#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

class AssetManager;

class Game {
private:
	bool isRunning;
	SDL_Window* window;
public:
	Game();
	~Game();
	static SDL_Renderer* renderer;
	static AssetManager* assetManager;
	static SDL_Event event;
	static SDL_Rect camera;

	int ticksLastFrame;

	void LoadLevel(int levelNumber);
	bool IsRunning() const;
	void Initialise(int width, int height);
	void ProcessInput();
	void Update();
	void Render();
	void Destroy();
	void HandleCameraMovement();
};