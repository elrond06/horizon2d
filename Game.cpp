#include <iostream>
#include "Constants.h"
#include "Game.h"
#include "glm/glm.hpp"
#include "EntityManager.h"
#include "Map.h"
#include "Components/TransformComponent.h"
#include "Components/SpriteComponent.h"
#include "Components/KeyboardControlComponent.h"

EntityManager manager;
SDL_Renderer* Game::renderer;
AssetManager* Game::assetManager = new AssetManager(&manager);
SDL_Event Game::event;
SDL_Rect Game::camera = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
Map* map;

Entity& player(manager.AddEntity("helicopter", LayerType::PLAYER_LAYER));

Game::Game() {
    this->isRunning = false;
}

Game::~Game() {

}

bool Game::IsRunning() const {
    return this->isRunning;
}

void Game::Initialise(int width, int height) {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::cerr << "Error initialising SDL." << std::endl;
        return;
    }

    //IMG_Init(IMG_INIT_PNG);

    window = SDL_CreateWindow(
        NULL,
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        width, height,
        SDL_WINDOW_BORDERLESS
    );

    if (!window) {
        std::cerr << "Error creating SDL window." << std::endl;
        return;
    }

    renderer = SDL_CreateRenderer(window, -1, 0);
    if (!renderer) {
        std::cerr << "Error creating SDL renderer." << std::endl;
        return;
    }

    LoadLevel(0);

    isRunning = true;
}

void Game::LoadLevel(int levelNumber) {
    // Load textures for entities
    assetManager->AddTexture("tank-image", std::string("./horizon2d/assets/images/tank-big-right.png").c_str());
    assetManager->AddTexture("helicopter-image", std::string("./horizon2d/assets/images/chopper-spritesheet.png").c_str());
    assetManager->AddTexture("radar-image", std::string("./horizon2d/assets/images/radar.png").c_str());
    assetManager->AddTexture("jungle-tile-texture", std::string("./horizon2d/assets/tilemaps/jungle.png").c_str());
    
    map = new Map("jungle-tile-texture", 2, 32);
    map->LoadMap("./horizon2d/assets/tilemaps/jungle.map", 25, 20);

    // Testing the Entity-Component System
    Entity& tankEntity(manager.AddEntity("tank", LayerType::ENEMY_LAYER));
    tankEntity.AddComponent<TransformComponent>(0, 500, 64, 0, 30, 30, 1);
    tankEntity.AddComponent<SpriteComponent>("tank-image");

    player.AddComponent<TransformComponent>(240, 106, 0, 0, 32, 32, 1);
    player.AddComponent<SpriteComponent>("helicopter-image", 2, 90, true, false);
    player.AddComponent<KeyboardControlComponent>("up", "right", "down", "left", "space");


    Entity& radarEntity(manager.AddEntity("radar", LayerType::UI_LAYER));
    radarEntity.AddComponent<TransformComponent>(720, 15, 0, 0, 64, 64, 1);
    radarEntity.AddComponent<SpriteComponent>("radar-image", 8, 150, false, true);
}

void Game::ProcessInput() {
    SDL_PollEvent(&event);

    switch (event.type) {
    case SDL_QUIT:
        isRunning = false;
        break;
    case SDL_KEYDOWN:
        if (event.key.keysym.sym == SDLK_ESCAPE)
            isRunning = false;
    default:
        break;
    }
}

void Game::Update() {
    // Ensure that faster computers wait untile FRAME_TARGET_TIME at least.
    while (!SDL_TICKS_PASSED(SDL_GetTicks(), ticksLastFrame + FRAME_TARGET_TIME));

    float deltaTime = (SDL_GetTicks() - ticksLastFrame) / 1000.0f;

    // Takes care of deltaTime become huge if we put a breakpoint.
    deltaTime = (deltaTime > 0.05f) ? 0.05f : deltaTime;

    ticksLastFrame = SDL_GetTicks();

    manager.Update(deltaTime);

    HandleCameraMovement();
}

void Game::Render() {
    SDL_SetRenderDrawColor(renderer, 21, 21, 21, 255);
    SDL_RenderClear(renderer);

    // If no entity present in EntityManger, stop rendering this frame.
    if (manager.HasNoEntity()) {
        return;
    }
    
    manager.Render();

    SDL_RenderPresent(renderer);
}

void Game::Destroy() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
}

void Game::HandleCameraMovement() {
    TransformComponent* mainPlayerTransform = player.GetComponent<TransformComponent>();

    camera.x = mainPlayerTransform->position.x - (WINDOW_WIDTH / 2);
    camera.y = mainPlayerTransform->position.y - (WINDOW_HEIGHT / 2);

    camera.x = camera.x < 0 ? 0 : camera.x;
    camera.y = camera.y < 0 ? 0 : camera.y;

    camera.x = camera.x > camera.w ? camera.w : camera.x;
    camera.y = camera.y > camera.h ? camera.h : camera.y;
}
